#ifndef RX5808_H
#define RX5808_H

#include "main.h"

#define NUMBER_CH       4
#define SIZE_NAME       10
#define LOW_FR_SCAN_LB  5300
#define LOW_FR_SCAN     5650
#define HI_FR_SCAN      5950

enum{
  CH1=0,
  CH2,
  CH3,
  CH4
};

typedef struct{
  uint8_t enable;
  uint8_t changing_frequency;
  uint8_t name[SIZE_NAME];
  uint16_t frequency;
  uint32_t rssi_max;
  uint32_t rssi;
  uint32_t ad_average;
  uint32_t ad_scan;
  uint32_t ad;
  uint32_t ad_min;
  uint32_t ad_max;
}rx5808_t;

typedef struct{
  uint16_t vbat;
  rx5808_t ch[NUMBER_CH];
  uint16_t ad[5];
}rx5808s_t;

extern rx5808s_t rx;

void rx_init(void);
void rx_send_frequency(uint16_t fr_, uint8_t* buffer, uint8_t ch);
void rx_send(uint8_t address_, uint8_t* tx_data, uint8_t ch);
void rx_receive(uint8_t address_, uint8_t* rx_data, uint8_t ch);
void rx_scan(uint8_t ch);
void  rx_ad_averege(uint8_t mode);

#endif