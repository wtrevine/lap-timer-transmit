#ifndef LORA_H
#define	LORA_H

#include "main.h"

enum {
  TX_NULL = 0,
  TX_CH1,
  TX_CH2,
  TX_CH3,
  TX_CH4,
  TX_CONF_CH1,
  TX_CONF_CH2,
  TX_CONF_CH3,
  TX_CONF_CH4,
  TX_TIME_CH1,
  TX_TIME_CH2,
  TX_TIME_CH3,
  TX_TIME_CH4,
  TX_REC_CH1,
  TX_REC_CH2,
  TX_REC_CH3,
  TX_REC_CH4,
  TX_TIMES,
  TX_RSSI,
  TX_ACK
};

enum {
  RX_NULL = 0,
  RX_CH1,
  RX_CH2,
  RX_CH3,
  RX_CH4,
  RX_SYS,
  RX_ACK
};

enum {
  P_CH = 0,
  P_CONF,
  P_LAP,
  P_REC,
  P_TIME,
  P_RSSI
};

typedef struct{
  uint8_t frequency[2];
  uint8_t trigger_in;
  uint8_t trigger_out;
}rx_ch_t;

typedef struct{
  uint8_t reset;
  uint8_t timer_lap_min[3];
  uint8_t timer_lap_max[3];
  uint8_t timer_min_reentry[3];
}rx_sys_t;

typedef union{
  rx_ch_t rx_ch;
  rx_sys_t rx_sys;
  uint8_t request;
}rx_t;

typedef struct{
  uint8_t address;
  uint8_t lixo;
  rx_t rx;
}rx_buffer_t;

void lora_init(void);
void lora_send(uint8_t address_, uint8_t tx_data);
void lora_send_array(uint8_t address_, uint8_t* tx_data, uint8_t size);
uint8_t lora_read(uint8_t address_);
void lora_read_array(uint8_t address_, uint8_t* rx_data, uint8_t size);
uint8_t lora_transmit(uint8_t type);
void lora_update_variables(void);
void lora_receive(uint8_t valid);
void lora_check_receive(void);
void lora_tx_set(uint8_t type, uint8_t ch);
void lora_process(void);

#endif