#ifndef COUNTERS_H
#define	COUNTERS_H

#include "main.h"

#define NUMBER_CH                       4
#define TIMEOUT_MIN_LAP                 10000

/* Timeout 1 MILLISECONDS */
#define TIMEOUT_SEND_TIME               5000
#define TIMEOUT_MIN_REENTRY             1000                
#define TIMEOUT_SEND                    100
#define TIMEOUT_READ                    2000 //300
#define TIMEOUT_RECEIVE                 5000

/* Timeout 1 SECONDS */
//#define TIMEOUT_KEEPALIVE           3600//7200   // Timeout para envio de keepalive (12 horas)

#define TRUE    1
#define FALSE   0

typedef enum type_time {
  SECONDS = 0,
  MILLISECONDS
} type_time_t;

typedef struct {
  uint8_t enable;
  uint8_t overflow;
  uint16_t count;
  uint16_t count_max;
  uint16_t count_overflow;
} timeout_t;

typedef struct {
  timeout_t send_time;
  timeout_t min_reentry[NUMBER_CH];
  timeout_t min_lap[NUMBER_CH];
  timeout_t send;
  timeout_t read;
  timeout_t receive;
}counter_t;

extern volatile counter_t counter;

void counters_reset(volatile timeout_t * time, uint8_t enable);
void counters_proccess(volatile timeout_t * sData, uint8_t ReStart);
void counters_interrupt(uint8_t type);
void counters_init();
void counters_overflow_proccess(void);

#endif