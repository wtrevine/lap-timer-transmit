/* USER CODE BEGIN Header */
/**
******************************************************************************
* @file           : main.h
* @brief          : Header for main.c file.
*                   This file contains the common defines of the application.
******************************************************************************
* @attention
*
* <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
* All rights reserved.</center></h2>
*
* This software component is licensed by ST under BSD 3-Clause license,
* the "License"; You may not use this file except in compliance with the
* License. You may obtain a copy of the License at:
*                        opensource.org/licenses/BSD-3-Clause
*
******************************************************************************
*/
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "adc.h"
#include "dma.h"
#include "spi.h"
#include "gpio.h"
#include "counters.h"
#include "rx5808.h"
#include "laptimer.h"
#include "lora.h"
  
extern uint16_t  erro;
extern uint32_t frequency, data;
extern uint8_t buffer_tx[5], buffer_rx[4], address, module;
extern uint8_t envia, envia_fr, recebe, scan;

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
  
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
  
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
  
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define EN_CH2_Pin GPIO_PIN_7
#define EN_CH2_GPIO_Port GPIOA
#define EN_CH3_Pin GPIO_PIN_0
#define EN_CH3_GPIO_Port GPIOB
#define EN_CH4_Pin GPIO_PIN_1
#define EN_CH4_GPIO_Port GPIOB
#define EN_CH1_Pin GPIO_PIN_10
#define EN_CH1_GPIO_Port GPIOA
#define DIO_Pin GPIO_PIN_11
#define DIO_GPIO_Port GPIOA
#define EN_LORA_Pin GPIO_PIN_12
#define EN_LORA_GPIO_Port GPIOA
#define RESET_LORA_Pin GPIO_PIN_15
#define RESET_LORA_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */
  
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
