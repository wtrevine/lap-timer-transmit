#ifndef LAP_TIMER_H
#define	LAP_TIMER_H

#include "main.h"

#define NUMBER_LAP              10
#define NUMBER_CH               4
#define TIMER_LAP_MIN           10000
#define TIMER_LAP_MAX           60000
#define TIMER_MIN_REENTRY       3000
#define TRIGGER_IN              85
#define TRIGGER_OUT             60

typedef enum {
  LORA_IDLE = 0,
  LORA_RECEIVE,
  LORA_RECEIVING,
  LORA_SEND,
  LORA_SENDING,
} lora_status_t;

typedef struct {
  uint8_t trigger_in;
  uint8_t trigger_out;
  uint8_t count_lap;
  uint8_t lap;
  uint8_t lap_closed;
  uint8_t flag_out_zone;
  uint8_t flag_min_lap;
  uint8_t flag_min_reentry;
  uint32_t lap_timer[NUMBER_LAP];
  uint32_t record;
  uint32_t record_3;
  uint32_t timer;
  uint8_t pause;
  uint8_t update;
}timer_t;

typedef struct {
  uint8_t send;
  uint8_t read;
  uint8_t retrans;
} error_t;

typedef struct {
  uint8_t ch;
  uint8_t conf;
  uint8_t lap;
  uint8_t rec;
  uint8_t time;
  uint8_t rssi;
} tx_t;

typedef struct {
  timer_t channel[NUMBER_CH];
  uint32_t timer_lap_min;
  uint32_t timer_lap_max;
  uint32_t timer_min_reentry;
  uint32_t timer_1s;
  uint32_t timer;
  lora_status_t status;
  error_t error;
  uint8_t send;
  tx_t tx;
  uint8_t reset;
  uint8_t receive;
  uint8_t beep;
  uint8_t retrans;
  uint8_t transmit;
}lap_timer_t;

extern lap_timer_t lt;

void laptimer_init(void);
void laptimer_process(void);

#endif