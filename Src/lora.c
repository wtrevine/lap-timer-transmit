#include "lora.h"

rx_buffer_t rx_buffer;
uint8_t buf_tx[50], size_buf;


void reset_ch(uint8_t ch)
{
  uint8_t trigger_in, trigger_out;
  
  trigger_in = lt.channel[ch].trigger_in;
  trigger_out = lt.channel[ch].trigger_out;
  memset(&lt.channel[ch], 0, sizeof(timer_t));
  lt.channel[ch].trigger_in = trigger_in;
  lt.channel[ch].trigger_out = trigger_out;
  
  lora_tx_set(P_CH, ch);
  lora_tx_set(P_REC, ch);
  lora_tx_set(P_CONF, ch);
}
uint32_t convert_24_data(uint8_t* buffer)
{
  return (*buffer << 16) | (*(buffer + 1) << 8) | *(buffer + 2);
}

void set_en(uint8_t type)
{
  if(type)
    HAL_GPIO_WritePin(EN_LORA_GPIO_Port, EN_LORA_Pin, GPIO_PIN_SET);
  else
    HAL_GPIO_WritePin(EN_LORA_GPIO_Port, EN_LORA_Pin, GPIO_PIN_RESET);
}

void lora_init(void)
{
  //uint8_t tmp;
  
  HAL_GPIO_WritePin(RESET_LORA_GPIO_Port, RESET_LORA_Pin, GPIO_PIN_RESET);
  HAL_Delay(100);
  HAL_GPIO_WritePin(RESET_LORA_GPIO_Port, RESET_LORA_Pin, GPIO_PIN_SET);
  set_en(1);
  HAL_Delay(500);
  
  lora_send(0x01, 0x80);
  HAL_Delay(100);
  lora_send(0x01, 0x88);
  HAL_Delay(100);
  //lora_send(0x01, 0x89);
  
  lora_send(0x09, 0xFC);
  lora_send(0x0B, 0x0B);
  lora_send(0x0C, 0x23);
  
  lora_send(0x4d, 0x87); //Habilita PA "Amplificador de potencia"
  
  lora_send(0x1D, 0x78);
  lora_send(0x1E, 0xA7);
  lora_send(0x1F, 0xFF);

  lora_send(0x20, 0x00);
  lora_send(0x21, 0x0A);
  
  lt.status = LORA_IDLE;
  
  lora_tx_set(P_CH, CH1);
  lora_tx_set(P_CH, CH2);
  lora_tx_set(P_CH, CH3);
  lora_tx_set(P_CH, CH4);
  lora_tx_set(P_CONF, CH1);
  lora_tx_set(P_CONF, CH2);
  lora_tx_set(P_CONF, CH3);
  lora_tx_set(P_CONF, CH4);
  lora_tx_set(P_REC, CH1);
  lora_tx_set(P_REC, CH2);
  lora_tx_set(P_REC, CH3);
  lora_tx_set(P_REC, CH4);
  lora_tx_set(P_TIME, NULL);  
}

void lora_send(uint8_t address_, uint8_t tx_data)
{
  uint8_t data = 0x80 | address_;
  spi_config(SPI_LORA);
  set_en(0);
  HAL_SPI_Transmit(&hspi1, &data, 1, 100);
  HAL_SPI_Transmit(&hspi1, &tx_data, 1, 100);
  set_en(1);
}

void lora_send_array(uint8_t address_, uint8_t* tx_data, uint8_t size)
{
  //*tx_data = 0x80;
  //*tx_data =  *tx_data | address_;
  spi_config(SPI_LORA);
  address_ |= 0x80;
  set_en(0);
  HAL_SPI_Transmit(&hspi1, &address_, 1, 100);
  HAL_SPI_Transmit(&hspi1, tx_data, size, 200);
  set_en(1);
}

uint8_t lora_read(uint8_t address_)
{
  uint8_t rx_data[2];
  spi_config(SPI_LORA);
  rx_data[0] = 0x00;
  rx_data[0] = address_ & 0x7F;
  set_en(0);
  HAL_SPI_Receive(&hspi1, rx_data, 2, 100);
  set_en(1);
  return rx_data[1];
}

void lora_read_array(uint8_t address_, uint8_t * rx_data, uint8_t size)
{
  spi_config(SPI_LORA);
  *rx_data = 0x00;
  *rx_data = address_ & 0x7F;
  set_en(0);
  HAL_SPI_Receive(&hspi1, rx_data, size+1, 100);
  set_en(1);
}

uint8_t lora_transmit(uint8_t type)
{
  uint8_t ch=0xFF, ad, i;
  
  if(lt.retrans == false) {
    switch(type) {
    case TX_CH1:
      ch = CH1;
    case TX_CH2:
      if (ch == 0xff)
        ch = CH2;
    case TX_CH3:
      if (ch == 0xff)
        ch = CH3;
    case TX_CH4:
      if(ch == 0xff)
        ch = CH4;  
      memcpy(&buf_tx[0], &type, 1);
      memcpy(&buf_tx[1], &lt.channel[ch].lap, 1);
      for(i=0, ad=2; i<NUMBER_LAP; i++, ad += 3)
        memcpy(&buf_tx[ad], &lt.channel[ch].lap_timer[i], 3);
      size_buf = 32;
      lt.receive = true;
      break;
      
    case TX_CONF_CH1:
      ch = CH1;
    case TX_CONF_CH2:
      if(ch == 0xff)
        ch = CH2; 
    case TX_CONF_CH3:
      if(ch == 0xff)
        ch = CH3;
    case TX_CONF_CH4:
      if(ch == 0xff)
        ch = CH4;
      memcpy(&buf_tx[0], &type, 1);
      memcpy(&buf_tx[1], &rx.ch[ch].frequency, 2);
      memcpy(&buf_tx[3], &lt.channel[ch].trigger_in, 1);
      memcpy(&buf_tx[4], &lt.channel[ch].trigger_out, 1);
      memcpy(&buf_tx[5], &rx.ch[ch].name, 10);
      memcpy(&buf_tx[15], &rx.ch[ch].enable, 1);
      size_buf = 16;
      lt.receive = true;
      break;
      
    case TX_TIME_CH1:
      ch = CH1;
    case TX_TIME_CH2:
      if (ch == 0xff)
        ch = CH2;
    case TX_TIME_CH3:
      if (ch == 0xff)
        ch = CH3;
    case TX_TIME_CH4:
      if(ch == 0xff)
        ch = CH4;
      memcpy(&buf_tx[0], &type, 1);
      //Envia volta que j� esta fechada
      if(lt.channel[ch].lap_closed != lt.channel[ch].lap) { 
        lt.receive = true;
        memcpy(&buf_tx[1], &lt.channel[ch].lap_closed, 1);
        memcpy(&buf_tx[2], &lt.channel[ch].lap_timer[lt.channel[ch].lap_closed - 1], 3);
        if(++lt.channel[ch].lap_closed > 10)
          lt.channel[ch].lap_closed = 1;
      }
      //Envia volta atual
      else {
        memcpy(&buf_tx[1], &lt.channel[ch].lap, 1);
        memcpy(&buf_tx[2], &lt.channel[ch].lap_timer[lt.channel[ch].lap - 1], 3);
      }
      size_buf = 5;
      break;
      
    case TX_REC_CH1:
      ch = CH1;
    case TX_REC_CH2:
      if (ch == 0xff)
        ch = CH2;
    case TX_REC_CH3:
      if (ch == 0xff)
        ch = CH3;
    case TX_REC_CH4:
      if(ch == 0xff)
        ch = CH4;
      memcpy(&buf_tx[0], &type, 1);
      memcpy(&buf_tx[1], &lt.channel[ch].record, 3);
      memcpy(&buf_tx[4], &lt.channel[ch].record_3, 3);
      size_buf = 7;
      lt.receive = true;
      break;
      
    case TX_TIMES:
      memcpy(&buf_tx[0], &type, 1);
      memcpy(&buf_tx[1], &lt.timer_lap_min, 3);
      memcpy(&buf_tx[4], &lt.timer_lap_max, 3);
      memcpy(&buf_tx[7], &lt.timer_min_reentry, 3);
      size_buf = 10;
      lt.receive = true;
      break;
      
    case TX_RSSI:
      memcpy(&buf_tx[0], &type, 1);
      memcpy(&buf_tx[1], &rx.ch[CH1].rssi, 1);
      memcpy(&buf_tx[2], &rx.ch[CH1].rssi_max, 1);
      memcpy(&buf_tx[3], &rx.ch[CH2].rssi, 1);
      memcpy(&buf_tx[4], &rx.ch[CH2].rssi_max, 1);
      memcpy(&buf_tx[5], &rx.ch[CH3].rssi, 1);
      memcpy(&buf_tx[6], &rx.ch[CH3].rssi_max, 1);
      memcpy(&buf_tx[7], &rx.ch[CH4].rssi, 1);
      memcpy(&buf_tx[8], &rx.ch[CH4].rssi_max, 1);
      size_buf = 9;
      break;
      
    case TX_ACK:
      memcpy(&buf_tx[0], &type, 1);
      size_buf = 1;
      break;
      
    default:
      lt.error.send++;
      return 0;
    }
    
    if(lt.receive == true){
      buf_tx[0] |= 0x20;
      counters_reset(&counter.receive, true);
    }
    
    if(lt.beep) {
      buf_tx[0] |= 0x40;
      lt.beep--;
    }
  }
  else{
    lt.retrans = false;
    lt.error.retrans++;
  }
  
  lora_send(0x0D, 0x80);
  lora_send(0x01, 0x89);
  lora_send(0x22, size_buf);
  lora_send_array(0x00, buf_tx, size_buf);
  lora_send(0x01, 0x8B);
  return 1;
}

void lora_update_variables(void) {
  uint8_t ch = 0xFF, i;
  
  // Solicita ACK
  if (rx_buffer.address & 0x20)
    lt.transmit = true;
  
  switch (rx_buffer.address & 0x1F) {
  case RX_CH1:
    ch = CH1;
  case RX_CH2:
    if (ch == 0xff)
      ch = CH2;
  case RX_CH3:
    if (ch == 0xff)
      ch = CH3;
  case RX_CH4:
    if (ch == 0xff)
      ch = CH4;
    memcpy(&rx.ch[ch].frequency, &rx_buffer.rx.rx_ch.frequency, 2);
    lt.channel[ch].trigger_in = rx_buffer.rx.rx_ch.trigger_in;
    lt.channel[ch].trigger_out = rx_buffer.rx.rx_ch.trigger_out;
    
    if(rx.ch[ch].frequency == 5999)
      rx_scan(ch);
    else
      rx_send_frequency(rx.ch[ch].frequency, &buffer_tx[0], ch);
    lora_tx_set(P_CONF, ch);
    lora_tx_set(P_CH, ch);
    break;
    
  case RX_SYS:
    lt.reset = rx_buffer.rx.rx_sys.reset;
    memcpy(&lt.timer_lap_min, &rx_buffer.rx.rx_sys.timer_lap_min, 3);
    memcpy(&lt.timer_lap_max, &rx_buffer.rx.rx_sys.timer_lap_max, 3);
    memcpy(&lt.timer_min_reentry, &rx_buffer.rx.rx_sys.timer_min_reentry, 3);
    for(i=0; i<NUMBER_CH; i++) {
      counter.min_reentry[i].count_max = lt.timer_min_reentry;
      counter.min_lap[i].count_max = lt.timer_lap_min;
    }
    lora_tx_set(P_TIME, NULL);
    if(lt.reset & 0x01)
      reset_ch(CH1);
    if(lt.reset & 0x02)
      reset_ch(CH2);
    if(lt.reset & 0x04)
      reset_ch(CH3);
    if(lt.reset & 0x08)
      reset_ch(CH4);
    break;
    
  case RX_ACK:
     lt.receive = false;
    if(rx_buffer.rx.request == true){
      lora_tx_set(P_CH, CH1);
      lora_tx_set(P_CH, CH2);
      lora_tx_set(P_CH, CH3);
      lora_tx_set(P_CH, CH4);
      lora_tx_set(P_CONF, CH1);
      lora_tx_set(P_CONF, CH2);
      lora_tx_set(P_CONF, CH3);
      lora_tx_set(P_CONF, CH4);
      lora_tx_set(P_REC, CH1);
      lora_tx_set(P_REC, CH2);
      lora_tx_set(P_REC, CH3);
      lora_tx_set(P_REC, CH4);
      lora_tx_set(P_TIME, NULL);
    }
    break;
    
  default:
    lt.error.read++;
  }
}

void lora_receive(uint8_t valid)
{
  uint8_t size, add;
  
  size = lora_read(0x13);
  add = lora_read(0x10);
  lora_send(0x0D, add);
  lora_read_array(0x00, (uint8_t*)&rx_buffer, size);
  if (valid)
    lora_update_variables();
}

void lora_check_receive(void)
{
  uint8_t value;
  
  value = lora_read(0x12);
  if (value & 0x40){
    if (value & 0x20) //crc error
      goto bora_error;
    if (value & 0x10){
      lora_receive(1);
      //lt.receive = false; //n�o era para estar aqui
      goto bora_ok;
    }
  }
bora_error:
  lora_receive(0);
  lt.error.read++;
bora_ok:
  lora_send(0x12, 0xFF);   
}

void lora_tx_set(uint8_t type, uint8_t ch)
{
  uint8_t *p;
  switch(type) {
  case P_CH:
    p = &lt.tx.ch;
    break;
  case P_CONF:
    p = &lt.tx.conf;
    break;
  case P_LAP:
    p = &lt.tx.lap;
    break;
  case P_REC:
    p = &lt.tx.rec;
    break;
  case P_TIME:
    lt.tx.time = 1;
    return;
  case P_RSSI:
    lt.tx.rssi = 1;
    return;
  default:
    return;
  }
  
  switch(ch){
  case CH1:
    *p |= 0x01; 
    break;
  case CH2:
    *p |= 0x02;
    break;
  case CH3:
    *p |= 0x04;
    break;
  case CH4:
    *p |= 0x08;
    break;
  }
}

void lora_process(void)
{
  switch (lt.status) {
  case LORA_IDLE:
    if(lt.transmit == true){
      lt.send = TX_ACK;
      lt.status = LORA_SEND;
      lt.transmit = false;
    }
    else if(counter.send.enable == false && counter.send.count_overflow == false)
      counters_reset(&counter.send, true);
    break;
    
  case LORA_RECEIVE:
    counters_reset(&counter.read, true);
    lora_send(0x01, 0x8D);
    lora_send(0x40, 0x00);
    lt.status = LORA_RECEIVING;
    break;
    
  case LORA_RECEIVING:
    if (HAL_GPIO_ReadPin(DIO_GPIO_Port, DIO_Pin)) {
      counters_reset(&counter.read, false);
      lora_check_receive();
      lora_send(0x01, 0x88);
      lt.status = LORA_IDLE;
    }
    break;
    
  case LORA_SEND:
    lora_send(0x01, 0x89);
    lora_send(0x40, 0x40);
    if (lora_transmit(lt.send))
      lt.status = LORA_SENDING;
    else
      lt.status = LORA_IDLE;
    break;
    
  case LORA_SENDING:
    if (HAL_GPIO_ReadPin(DIO_GPIO_Port, DIO_Pin)) {
      if ((lora_read(0x12) & 0x08) == 0)
        lt.error.send++;
      lora_send(18, 0xFF);
      lt.send = TX_NULL;
      if(lt.receive == true){
        lt.status = LORA_RECEIVE;
        //lt.receive = false;
      }
      else
        lt.status = LORA_IDLE;
    }
    break;
  }
}
