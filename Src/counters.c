/*includes*/
#include "counters.h"

extern uint8_t transm_lora;
extern uint8_t recebe_lora;
extern uint8_t read_all;

/* temporizaores*/
volatile counter_t counter;

/*
* Reseta temporizador
*/
void counters_reset(volatile timeout_t * time, uint8_t enable) {
  time->count = 0;
  time->enable = enable;
  time->overflow = FALSE;
}

/*
* Processa temporizador
*/
void counters_proccess(volatile timeout_t * sData, uint8_t ReStart) {
  if (sData->enable == TRUE) {
    if ((++sData->count) >= sData->count_max) {
      sData->count = 0;
      sData->overflow = TRUE;
      sData->enable = ReStart;
    }
  } else
    sData->count = 0;
}

/*
* Chama processos
*  */
void counters_interrupt(uint8_t type) {
  uint8_t i;
  
  switch (type) {
  case SECONDS:
    //counters_proccess(&timeout_keepalive, TRUE);
    
    break;
    
  case MILLISECONDS:
    counters_proccess(&counter.send_time, TRUE);
    for(i=0; i<NUMBER_CH; i++) {
      counters_proccess(&counter.min_reentry[i], FALSE);
      counters_proccess(&counter.min_lap[i], FALSE);
    }
    counters_proccess(&counter.send, FALSE);
    counters_proccess(&counter.read, FALSE);
    counters_proccess(&counter.receive, TRUE);
    break;
  }
}

/*
* Inicia temporizadores
*/
void counters_init() {
  uint8_t i;
  
  counter.send_time.count_max = TIMEOUT_SEND_TIME;
  for(i=0; i<NUMBER_CH; i++) {
    counter.min_reentry[i].count_max = TIMEOUT_MIN_REENTRY;
    counter.min_lap[i].count_max = TIMEOUT_MIN_LAP;
  }
  counter.send.count_max = TIMEOUT_SEND;
  counter.read.count_max = TIMEOUT_READ;
  counter.send.count_max = TIMEOUT_SEND;
  counter.receive.count_max = TIMEOUT_RECEIVE;
  
  counters_reset(&counter.send_time, TRUE);
  counters_reset(&counter.receive, TRUE);
}

/* 
* Verifica estoutoros dos temporizadores
*/
void counters_overflow_proccess(void) {
  uint8_t i;
  
  if (counter.send_time.overflow == TRUE) {
    counter.send_time.overflow = FALSE;
    //lt.timer_1s++;
    
    for(i=0; i<NUMBER_CH; i++) {
      if(lt.channel[i].update != 0) {
        lora_tx_set(P_LAP, i);
        if(lt.channel[i].update == 2)
          lt.channel[i].update = 0;
      }
    } 
  }
  
  for(i=0; i<NUMBER_CH; i++) {
    if (counter.min_reentry[i].overflow == TRUE) {
      counter.min_reentry[i].overflow = FALSE;
      lt.channel[i].flag_min_reentry = false;
    }
    if (counter.min_lap[i].overflow == TRUE) {
      counter.min_lap[i].overflow = FALSE;
      lt.channel[i].flag_min_lap = false;
      rx.ch[i].rssi_max = 0;
    }
  }
  
  if (counter.receive.overflow == TRUE) {
    counter.receive.overflow = FALSE;
    lora_tx_set(P_RSSI, NULL); //Envia qualquer coisa para receber
    if(lt.status == LORA_IDLE)
      lt.receive = true;
  }
  
  if (counter.send.overflow == TRUE) {
    counter.send.overflow = FALSE;
    if(lt.status == LORA_IDLE){
      if(lt.tx.ch != 0) {
        if(lt.tx.ch & 0x01) {lt.send = TX_CH1; lt.tx.ch &= 0xFE;}
        else if(lt.tx.ch & 0x02) {lt.send = TX_CH2; lt.tx.ch &= 0xFD;}
        else if(lt.tx.ch & 0x04) {lt.send = TX_CH3; lt.tx.ch &= 0xFB;}
        else if(lt.tx.ch & 0x08) {lt.send = TX_CH4; lt.tx.ch &= 0xF7;}
        lt.status = LORA_SEND;
      }
      else if(lt.tx.rec != 0) {
        if(lt.tx.rec & 0x01) {lt.send = TX_REC_CH1; lt.tx.rec &= 0xFE;}
        else if(lt.tx.rec & 0x02) {lt.send = TX_REC_CH2; lt.tx.rec &= 0xFD;}
        else if(lt.tx.rec & 0x04) {lt.send = TX_REC_CH3; lt.tx.rec &= 0xFB;}
        else if(lt.tx.rec & 0x08) {lt.send = TX_REC_CH4; lt.tx.rec &= 0xF7;}
        lt.status = LORA_SEND;
      }
      else if(lt.tx.time != 0) {
        lt.status = LORA_SEND;
        lt.send = TX_TIMES;
        lt.tx.time = 0;
      }
      else if(lt.tx.conf != 0) {
        if(lt.tx.conf & 0x01) {lt.send = TX_CONF_CH1; lt.tx.conf &= 0xFE;}
        else if(lt.tx.conf & 0x02) {lt.send = TX_CONF_CH2; lt.tx.conf &= 0xFD;}
        else if(lt.tx.conf & 0x04) {lt.send = TX_CONF_CH3; lt.tx.conf &= 0xFB;}
        else if(lt.tx.conf & 0x08) {lt.send = TX_CONF_CH4; lt.tx.conf &= 0xF7;}
        lt.status = LORA_SEND;
      }
      else if(lt.tx.lap != 0) {
        if(lt.tx.lap & 0x01) {lt.send = TX_TIME_CH1; lt.tx.lap &= 0xFE;}
        else if(lt.tx.lap & 0x02) {lt.send = TX_TIME_CH2; lt.tx.lap &= 0xFD;}
        else if(lt.tx.lap & 0x04) {lt.send = TX_TIME_CH3; lt.tx.lap &= 0xFB;}
        else if(lt.tx.lap & 0x08) {lt.send = TX_TIME_CH4; lt.tx.lap &= 0xF7;}
        lt.status = LORA_SEND;
      }
      else if(lt.tx.rssi != 0) {
        lt.status = LORA_SEND;
        lt.send = TX_RSSI;
        lt.tx.rssi = 0;
      }
    }
  }
  
  if (counter.read.overflow == TRUE) {
    counter.read.overflow = FALSE;
    lt.status = LORA_SEND;
    lt.retrans = true;
    lora_send(0x01, 0x88);
    lora_send(0x12, 0xFF);
  }
  

}
