#include "laptimer.h"

lap_timer_t lt;

void best_3_lap(timer_t * timer, uint8_t ch)
{
  uint32_t record;
  
  if (timer->count_lap >= 3) {
    if (timer->lap == 1)
      record = timer->lap_timer[9] + timer->lap_timer[8] + timer->lap_timer[7];
    else if (timer->lap == 2)
      record = timer->lap_timer[0] + timer->lap_timer[9] + timer->lap_timer[8];
    else if (timer->lap == 3)
      record = timer->lap_timer[1] + timer->lap_timer[0] + timer->lap_timer[9];
    else if (timer->lap >= 4)
      record = timer->lap_timer[timer->lap - 2] + timer->lap_timer[timer->lap - 3] + timer->lap_timer[timer->lap - 4];
    
    if (record < timer->record_3 || timer->record_3 == 0) {
      timer->record_3 = record;
      lora_tx_set(P_REC, ch);
    }
  }
}

void real_timer(uint8_t ch)
{
  if(lt.channel[ch].pause == true)
    return;
  
  if(rx.ch[ch].changing_frequency == true)
    return;
  
  //Atualiza tempo em real time
  if(lt.channel[ch].lap <= 10 && lt.channel[ch].lap > 0 && lt.timer_lap_max > (lt.timer - lt.channel[ch].timer)) {
    lt.channel[ch].lap_timer[lt.channel[ch].lap - 1] =  (lt.timer - lt.channel[ch].timer);
    lt.channel[ch].update = 1;
  }
  //Zera tempo se estouro de tempo
  else if(lt.channel[ch].lap <= 10 && lt.channel[ch].lap > 0 && lt.timer_lap_max <= (lt.timer - lt.channel[ch].timer) && lt.channel[ch].update != 0){
    lt.channel[ch].lap_timer[lt.channel[ch].lap - 1] = 0;
    lt.channel[ch].update = 2;
  }
}

void process_channel(uint8_t ch)
{
  if(lt.channel[ch].pause == true)
    return;
  
  if(rx.ch[ch].changing_frequency == true)
    return;
  
  if (lt.channel[ch].lap > NUMBER_LAP)
    lt.channel[ch].lap = 1;
  
  if ((rx.ch[ch].rssi > lt.channel[ch].trigger_in) && lt.channel[ch].flag_out_zone == true && lt.channel[ch].flag_min_reentry  == false) {
    lt.channel[ch].flag_out_zone = false;
    lt.channel[ch].flag_min_reentry = true;
    counters_reset(&counter.min_reentry[ch], true);
    
    lt.beep++;
    
    if (lt.channel[ch].timer == 0) {
      lt.channel[ch].lap++;
      lt.channel[ch].lap_closed++;
    }
    //Valida se � passagem dentro dos tempos de limite
    else if (lt.channel[ch].flag_min_lap == false && lt.timer_lap_max > (lt.timer - lt.channel[ch].timer)) {
      //Passagem valida
      lt.channel[ch].lap_timer[lt.channel[ch].lap - 1] =  (lt.timer - lt.channel[ch].timer);
      
      //Zera campo da frente
      if (lt.channel[ch].lap <= 9) {
        lt.channel[ch].lap_timer[lt.channel[ch].lap] = 0;
      }
      else if (lt.channel[ch].lap == 10) {
        lt.channel[ch].lap_timer[0] = 0;
      }
      
      //verifica record
      if (lt.channel[ch].record == 0){
        lt.channel[ch].record = lt.channel[ch].lap_timer[lt.channel[ch].lap - 1];
        lora_tx_set(P_REC, ch);
      }
      else if (lt.channel[ch].lap_timer[lt.channel[ch].lap - 1] < lt.channel[ch].record){
        lt.channel[ch].record = lt.channel[ch].lap_timer[lt.channel[ch].lap - 1];
        lora_tx_set(P_REC, ch);
      }
      
      lt.channel[ch].lap_closed = lt.channel[ch].lap; 
      lt.channel[ch].lap++;
      lt.channel[ch].count_lap++;
      best_3_lap(&lt.channel[ch], ch); //melhor_3_voltas(++cont_volta);
    }
    else {
      lt.channel[ch].count_lap = 0; //cont_volta = 0;
    }
    lt.channel[ch].timer = lt.timer;
    //lt.timer_lap_min = 0;
    lt.channel[ch].flag_min_lap = true;
    counters_reset(&counter.min_lap[ch], true);
  }
  
  if ((rx.ch[ch].rssi < lt.channel[ch].trigger_out) && lt.channel[ch].flag_out_zone == false)
    lt.channel[ch].flag_out_zone = true;
}

void laptimer_init(void)
{
  uint8_t i;
  
  lt.timer_lap_min = TIMER_LAP_MIN;
  lt.timer_lap_max = TIMER_LAP_MAX;
  lt.timer_min_reentry = TIMER_MIN_REENTRY;
  for(i=0; i<NUMBER_CH; i++) {
    lt.channel[i].trigger_in = TRIGGER_IN;
    lt.channel[i].trigger_out = TRIGGER_OUT;
  }
  lt.beep = 0;
}

void laptimer_process(void)
{
  uint8_t i;
  
  for(i=0; i<NUMBER_CH; i++) {
    process_channel(i);
    real_timer(i);
  }
}
