#include "rx5808.h"

rx5808s_t rx;

uint16_t convert_frequency(uint16_t fr)
{
  uint16_t a, b;
  
  a = (fr - 479) / 2;
  b = a & 0x001f;
  a = a & 0xffe0;
  a = a << 2;
  return a + b;
}

void set_ch(uint8_t set, uint8_t ch)
{
  switch(ch){
  case CH1:
    if(set)
      HAL_GPIO_WritePin(EN_CH1_GPIO_Port, EN_CH1_Pin, GPIO_PIN_SET);
    else
      HAL_GPIO_WritePin(EN_CH1_GPIO_Port, EN_CH1_Pin, GPIO_PIN_RESET);
    break;
    
  case CH2:
    if(set)
      HAL_GPIO_WritePin(EN_CH2_GPIO_Port, EN_CH2_Pin, GPIO_PIN_SET);
    else
      HAL_GPIO_WritePin(EN_CH2_GPIO_Port, EN_CH2_Pin, GPIO_PIN_RESET);
    break;
    
  case CH3:
    if(set)
      HAL_GPIO_WritePin(EN_CH3_GPIO_Port, EN_CH3_Pin, GPIO_PIN_SET);
    else
      HAL_GPIO_WritePin(EN_CH3_GPIO_Port, EN_CH3_Pin, GPIO_PIN_RESET);
    break;
    
  case CH4:
    if(set)
      HAL_GPIO_WritePin(EN_CH4_GPIO_Port, EN_CH4_Pin, GPIO_PIN_SET);
    else
      HAL_GPIO_WritePin(EN_CH4_GPIO_Port, EN_CH4_Pin, GPIO_PIN_RESET);
    break;
  }
}

void set_ad_min_max(uint8_t i)
{
  //Atualiza valores de limite
  if(rx.ch[i].ad_min == 0 && rx.ch[i].ad_max == 0) {
    rx.ch[i].ad_min = rx.ch[i].ad;
    rx.ch[i].ad_max = rx.ch[i].ad;
  }
  if(rx.ch[i].ad < rx.ch[i].ad_min)
    rx.ch[i].ad_min = rx.ch[i].ad;
  if(rx.ch[i].ad > rx.ch[i].ad_max)
    rx.ch[i].ad_max = rx.ch[i].ad;
}

void rx_init(void)
{
  set_ch(1, CH1);
  set_ch(1, CH3);
  set_ch(1, CH2);
  set_ch(1, CH4);
  
  lt.channel[CH1].pause = true;
  lt.channel[CH2].pause = true;
  lt.channel[CH3].pause = true;
  lt.channel[CH4].pause = true;
}

void rx_send_frequency(uint16_t fr_, uint8_t* buffer, uint8_t ch)
{
  uint32_t data_;
  
  lt.channel[ch].pause = true;
  spi_config(SPI_RX);  
  data_ = convert_frequency(fr_);
  data_ = (data_ << 5) + 0x11;
  
  buffer[0] = data_ & 0x1f;
  buffer[1] = (data_ >> 5) & 0x1f;
  buffer[2] = (data_ >> 10) & 0x1f;
  buffer[3] = (data_ >> 15) & 0x1f;
  buffer[4] = (data_ >> 20) & 0x1f;
  
  set_ch(0, ch);
  HAL_SPI_Transmit(&hspi1, &buffer[0], 5, 10);
  set_ch(1, ch);
  
  rx.ch[ch].frequency = fr_;
  lt.channel[ch].pause = false;
}

void rx_send(uint8_t address_, uint8_t* tx_data, uint8_t ch)
{
  uint8_t buffer[5];
  
  spi_config(SPI_RX);
  buffer[0] = address_ | 0x10;
  buffer[1] = *tx_data;
  buffer[2] = *tx_data + 1;
  buffer[3] = *tx_data + 2;
  buffer[4] = *tx_data + 3;
  
  set_ch(0, ch);
  HAL_SPI_Transmit(&hspi1, &buffer[0], 5, 10);
  set_ch(1, ch);
}

void rx_receive(uint8_t address_, uint8_t* rx_data, uint8_t ch)
{
  spi_config(SPI_RX);
  set_ch(0, ch);
  HAL_SPI_Transmit(&hspi1, &address_, 1, 10);
  HAL_SPI_Receive(&hspi1, rx_data, 4, 10);
  set_ch(1, ch);
}

void rx_scan(uint8_t ch)
{
  uint16_t i, good_fr;
  
  rx.ch[ch].ad_scan = 0;
  
  for(i=LOW_FR_SCAN; i <= HI_FR_SCAN; i+=2) {
    rx.ch[ch].changing_frequency = true;
    rx_send_frequency(i, &buffer_tx[0], ch);        
    HAL_Delay(35);
    rx.ch[ch].changing_frequency = false;
    HAL_Delay(5);
    rx_receive(1, &buffer_rx[0], ch);
    
    if(0 != memcmp(&buffer_tx[1], &buffer_rx[0], 4))
      erro++;
    
    if(rx.ch[ch].ad_scan < rx.ch[ch].ad) {
      rx.ch[ch].ad_scan = rx.ch[ch].ad;
      good_fr = i;
    }
  }
  rx_send_frequency(good_fr, &buffer_tx[0], ch);
}

void  rx_ad_averege(uint8_t mode)
{
  uint8_t i;
  
  for(i=0; i<NUMBER_CH; i++) {
    if(mode) {
      rx.ch[i].ad_average += rx.ad[i+1];
    }
    else {
      if(rx.ch[i].changing_frequency == false) {
        rx.ch[i].ad = rx.ch[i].ad_average / 10;
        set_ad_min_max(i);
        rx.ch[i].rssi = ((rx.ch[i].ad - rx.ch[i].ad_min) * 100) / ((rx.ch[i].ad_max - rx.ch[i].ad_min));
        //rx.ch[i].rssi = (rx.ch[i].ad - rx.ch[i].ad_min) / ((rx.ch[i].ad_max - rx.ch[i].ad_min)/100);
        //Atualiza rssi max
        if (rx.ch[i].rssi > rx.ch[i].rssi_max)
          rx.ch[i].rssi_max = rx.ch[i].rssi;
      }
      rx.ch[i].ad_average = 0; 
    }
  }
}